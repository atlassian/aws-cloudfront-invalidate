# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.10.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 0.10.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.9.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.8.0

- minor: Internal maintenance: Update bitbucket-pipes-toolkit.

## 0.7.0

- minor: Internal maintenance: Bump version of the base Docker image to python:3.10-slim.
- patch: Internal maintenance: Bump version of boto3 and other dependencies.
- patch: Internal maintenance: Bump version of the bitbucket-pipe-release.
- patch: Internal maintenance: Update the community link.

## 0.6.0

- minor: Bump pipes-toolkit -> 2.2.0.

## 0.5.0

- minor: Bump boto3 version to 1.17.12.
- minor: Support AWS OIDC authentication. Environment variables AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY are not required anymore.

## 0.4.1

- patch: Internal maintenance: bump bitbucket-pipe-release with ssh push back approach.

## 0.4.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 0.3.3

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.3.2

- patch: Internal maintenance: Add gitignore secrets.

## 0.3.1

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.
- patch: Update the Readme with details about default variables.

## 0.3.0

- minor: Add default values for AWS variables.

## 0.2.0

- minor: Display a warning message when there is a new version of the pipe.

## 0.1.4

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.1.3

- patch: Internal maintenance: Add auto infrastructure for tests.

## 0.1.2

- patch: Documentation improvements to provide a detailed description and prerequisites for the pipe.

## 0.1.1

- patch: Improved the documentaion

## 0.1.0

- minor: Initial release
